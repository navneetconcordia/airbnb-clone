import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <div className="footer">
            <p>Airbnb clone | No rights reserved - This is a Demo</p>
            <p>Privacy · Terms · Sitemap · Company Detail</p>
        </div>
    );
};

export default Footer;