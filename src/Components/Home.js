import React from 'react';
import Banner from './Banner';
import './Home.css';
import Card from './Card';
import * as photo from './index';


const Home = () => {
    return (
        <div className="home">
            <Banner />

            <div class="home__section">
                <Card
                    src={photo.online_experiences}
                    title="Online Experiences"
                    description="Unique activities we can do together, led by a world of hosts."
                 />
                <Card
                    src={photo.unique_stays}
                    title="Unique stays"
                    description="Space that are more that just a place to sleep."
                 />
                <Card
                    src={photo.entire_home}
                    title="Entire Home"
                    description="Comfortable private place with room for friends or family."
                 />
            </div>
            <div class="home__section">
                <Card
                    src={photo.oldtown}
                    title="Heart of Oldtown"
                    description="Perfect for families this room sleeps 5 comfortably. It features a microwave, mini frig, keurig coffee pot and hot water dispenser for tea."
                    price="$249/ night"
                 />
                <Card
                    src={photo.cabin}
                    title="Entire cabin hosted"
                    description="The cabin is close to town, nestled on 10 acres overlooking the head of the Beluga Slough."
                    price="$149/ night"
                 />
                <Card
                    src={photo.boutique}
                    title="Room in boutique hotel hosted"
                    description="Seafarer Suites is conveniently located with Ocean views in the picturesque Historical Art and Food District of Homer."
                    price="$99/ night
                    "
                 />
            </div>
        </div>
    );
};

export default Home;