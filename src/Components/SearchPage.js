
import { Button } from '@material-ui/core';
import React from 'react';
import './SearchPage.css';
import SearchResult from './SearchResult';
import * as photo from './index';

const SearchPage = () => {
    return (
        <div className="searchPage">
            <div class="searchPage__info">
                <p>62 stays · 26 august to 30 august · 2 guest</p>
                <h1>Stay nearby</h1>
                <Button variant="outlined">Cancellation Flexibility</Button>
                <Button variant="outlined">Type of place</Button>
                <Button variant="outlined">Price</Button>
                <Button variant="outlined">Rooms and Beds</Button>
                <Button variant="outlined">More filters</Button>
            </div>
            <SearchResult
                img={photo.pic1}
                location="Private room in center of Toronto"
                title="Stay at this spacious House"
                description="1 guest · 1 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Kitchen · Free parking · Washing Machine"
                star={4.73}
                price="$60 / night"
                total="$234 total"
             />

<SearchResult
                img={photo.pic2}
                location="Private room in center of Toronto"
                title="Independant luxury studio apartment"
                description="2 guest · 3 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Kitchen"
                star={4.3}
                price="$80 / night"
                total="$299 total"
            />

            <SearchResult
                img={photo.pic3}
                location="Private room in center of Toronto"
                title="London Studio Apartments"
                description="4 guest · 4 bedroom · 4 bed · 2 bathrooms · Free parking · Washing Machine"
                star={3.8}
                price="$65 / night"
                total="$399 total"
            />
            <SearchResult
                img={photo.pic4}
                location="Private room in center of Toronto"
                title="30 mins to Dundas Street, Excel Toronto"
                description="1 guest · 1 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Kitchen · Free parking · Washing Machine"
                star={4.1}
                price="$99 / night"
                total="$499 total"
            />
            <SearchResult
                img={photo.pic5}
                location="Private room in center of Toronto"
                title="Spacious Peaceful Modern Bedroom"
                description="3 guest · 1 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Free parking · Dry Cleaning"
                star={5.0}
                price="$129 / night"
                total="$699 total"
            />
            <SearchResult
                img={photo.pic6}
                location="Private room in center of Toronto"
                title="The Blue Room In Toronto"
                description="2 guest · 1 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Washing Machine"
                star={4.23}
                price="$119 / night"
                total="$899 total"
            />
            <SearchResult
                img={photo.pic7}
                location="Niagara falls view from hotel room"
                title="5 Star Luxury Apartment"
                description="3 guest · 1 bedroom · 1 bed · 1.5 shared bthrooms · Wifi · Kitchen · Free parking · Washing Machine"
                star={4.92}
                price="$149 / night"
                total="$999 total"
            />
        </div>
    );
};

export default SearchPage;